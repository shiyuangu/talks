%%%%%
% author: S. Gu
% Comments:
% Incompatibility:
% enumitem: if used, no bullets
%%%%%
\documentclass[compress]{beamer}  %compress:make navigation bar as small as possible
\mode<presentation> {
  \usetheme{Warsaw}
  \usefonttheme[onlymath]{serif}

  \setbeamercovered{transparent}
  %\useoutertheme[footline=empty]{miniframes}
  %\setbeamertemplate{headline}[default]
  \setbeamertemplate{headline}{

  \leavevmode%

  \hbox{%

  \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.25ex,dp=1ex,left]{title in head/foot}%

    \usebeamerfont{title in head/foot}\centering\insertsection

  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.25ex,dp=1ex,right]{section in head/foot}%

    %\usebeamerfont{date in head/foot}\insertshortdate{}\hspace*{2em}

    %\insertframenumber{} / \inserttotalframenumber\hspace*{2ex} 

     %\insertpagenumber{}/$35$\hspace*{2ex}

       %\insertpagenumber{}\hspace*{2ex}
      %\usebeamerfont{section in head/foot}\insertpart

  \end{beamercolorbox}}%

  \vskip2pt%

}

  \setbeamertemplate{footline}{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
    \usebeamerfont{author in head/foot}\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.444444\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.222222\paperwidth,ht=2.25ex,dp=1ex,center]{date in head/foot}%
    %\usebeamerfont{date in head/foot}\insertshortdate{}\hspace*{2em}
    %\insertframenumber{} / \inserttotalframenumber\hspace*{2ex} 
    %\insertpagenumber{}/$35$\hspace*{2ex}
    %Argonne\hspace*{8ex}
    %\insertdate\hspace*{5ex}
    \usebeamerfont{date in head/foot}\insertshortdate
       %\insertpagenumber{}\hspace*{2ex}
  \end{beamercolorbox}}%
  \vskip0pt%
}
}
\setbeamertemplate{navigation symbols}{}
\usepackage{amsmath,amsthm,amssymb,latexsym,amscd}
\usepackage{mathabx} %for 	opdoteq
\usepackage{slashbox} %%for slash in tables
\usepackage{array}  %%for m{} in tabular
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{color}
\usepackage{tikz}
\usepackage{setspace}  %for linespacing
\usepackage{url}
\usepackage{multimedia}
\usepackage{animate}   %for animategraphics
\usepackage{scalefnt}  %for scalefont
\begin{document}
% Insert the following frame inside document
\title[MDV]{Component-based Isosurface Extraction for Multiple Dataset Visualization}
\author{Shiyuan Gu}
\date{}
\institute[ANL]{Argonne National Laboratory}
\begin{frame}
  \titlepage
\end{frame}

\begin{frame}[t]
  \frametitle{Isosurface Extraction}
  \begin{itemize}
  \item<1->  Goal:Given a scalar field $F(x)$, find x s.t.
    \[
       F(x)=isovalue
       \]
       Discrete sampling of F(x) gives a 3D array, which is called volumetric data.
     \item<2-> Most prevalent method: marching cubes
       \begin{center}
         \includegraphics[scale=0.3]{pics/marchingcube.jpg}
       \end{center}
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{Multiple Dataset Visualization}
  \begin{itemize}
  \item MDV processes multiple datasets of interest together so that the cross-correlations (i.e., differences and similarities) among them can be better explored.
    \vspace{0.1in}
    \begin{center}
    \begin{minipage}[c]{0.4\textwidth}
      \includegraphics[scale=0.3]{pics/mdvrds.jpg}
    \end{minipage}%
    \begin{minipage}[c]{0.4\textwidth}
      \includegraphics[scale=0.3]{pics/mdvnrds.jpg}
    \end{minipage}
     \begin{minipage}[c]{0.4\textwidth}
      \includegraphics[scale=0.3]{pics/mdvrdsh.jpg}
    \end{minipage}%
    \begin{minipage}[c]{0.4\textwidth}
      \includegraphics[scale=0.3]{pics/mdvnrdsh.jpg}
    \end{minipage}
   \end{center}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Approach}
  \begin{itemize}
  \item<1-> \emph{Data Coherence}: a measure of similarity between the datasets.
  \item<2-> Assume the similarity at the voxel level means the similarity in the geometry of the isosurfaces.
  \item<3-> Divide the datasets into two categories: reference datasets (RDS) and non-reference datasets(NRDS)
    \begin{itemize}
    \item<4-> For RDS, complete isosurface extraction is applied and the polygons are used subsequently to also represent parts of the isosurface of the non-referece datasets.
    \item<5-> For NRDS, isosurface extraction is applied only for those \emph{``regions''} which \emph{``significantly differ''} from the corresponding regions of the reference dataset; for the other regions, the polygons are retrieved from the reference dataset. 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Approach}
  \begin{center}
    \begin{minipage}[c]{0.5\textwidth}
       region=voxel?
      \includegraphics[scale=0.6]{pics/crack.jpg}\\
      \phantom{m}cracks in (Karki, Khanduja,06')
    \end{minipage}%
  \begin{minipage}[c]{0.5\textwidth}
     region=component?
      \includegraphics[scale=0.6]{pics/crackfree.jpg}\\
      \phantom{m}crackfree in our approach
  \end{minipage}
  \begin{minipage}[c]{0.5\textwidth}
  \end{minipage}
\end{center}
\end{frame}

\begin{frame}
  \frametitle{Component-Based Isosurface Extraction}
  \begin{center}
    \includegraphics[scale=0.3]{pics/components}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Component-Based Isosurface Extraction}
  \begin{itemize}
    \item Besides crack-free guarantee, Our new approach is also more effective in identifying interesting structure difference and suppressing noise. A small change in a big surface may not be of interest (due to noise or  too small to be visiually detectable).  
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Algorithm}
  \begin{center}
    \includegraphics[scale=0.3]{pics/slidealgorithm0.jpg}
  \end{center}
\end{frame}

\begin{frame}[t]
  \frametitle{Algorithm}
  {\footnotesize
  For NRDS:
  \begin{itemize}
  \item<1-> Compute the voxel difference (eight values $\rightarrow$ one values, mean, min/max?)
  \item<2-> Pick a unprocessed significant voxel ($voxel.diff>threashold1$) and find its component (depth first search)
  \item<3-> Extract the polygons of the component if the components contain sufficiently many voxels, i.e. $ nSV > threshold2\times nV$
   \item<4-> Retrieve the polygons of the component $c_i$ from RDS if no new polygons are exacted in corresponding regions of NRDS and
     \[
        nMis < threshold3\times nV
     \]
   \end{itemize}
   \vspace{-0.2in}\begin{center}
     \includegraphics[scale=0.4]{pics/overlap.jpg}
   \end{center}
 }
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{center}
    \includegraphics[scale=0.3]{pics/slideresult0.jpg}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{center}
    \includegraphics[scale=0.3]{pics/slideresult1.jpg}
  \end{center}
  \begin{thebibliography}{2}    
   \beamertemplatearticlebibitems
  \bibitem{gu}
    S.~Gu and B.~Karki. { \it Component-based isosurface extraction for multiple dataset
  visualization.} \newblock {\em Journal of WSCG }, 18:~97-104, 2010.
\end{thebibliography}
\end{frame}

% \begin{frame}
%   \begin{thebibliography}{2}    
%    \beamertemplatearticlebibitems
%   \bibitem{gu}
%     S.~Gu and B.~Karki. { \it Component-based isosurface extraction for multiple dataset
%   visualization.} \newblock {\em Journal of WSCG }, 18:~97-104, 2010.
% \end{thebibliography}
% \vspace{0.8in}
% \begin{center}
%   {\Huge Thank you.}
% \end{center}
% \end{frame}

\end{document}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
